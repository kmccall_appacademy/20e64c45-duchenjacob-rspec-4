
class Dictionary
  attr_accessor :entries

  def initialize
    @entries = {}
  end


  def add(entry)
    case entry
    when Hash then @entries.merge!(entry)
    when String then @entries.merge!(entry => nil)
    end
  end

  def keywords
    @entries.keys.sort
  end


    def include?(entry)
      keywords.include?(entry)
    end

    def find(string)
      @entries.select { |key, value| key.include?(string) }
    end
    def printable
      print_string = ""
      keywords.each { |key| print_string << %([#{key}] "#{@entries[key]}"\n) }

      print_string.chomp
    end
end

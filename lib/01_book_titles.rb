
class Book
  attr_reader :title

  def title=(phrase)
    stay_lowercase = %w(the a an and in of)

    words = phrase.downcase.split(" ")
    title = words.map do |word|
      if stay_lowercase.include?(word)
        word
      else
        word.capitalize
      end
    end

    title.first.capitalize!
    title.last.capitalize!

    @title = title.join(" ")
  end
end

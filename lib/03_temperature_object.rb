class Temperature
  def initialize(temperature)
    @temperature = temperature
  end

  def self.from_celsius(temperature)
    Temperature.new(c: temperature)
  end

  def self.from_fahrenheit(temperature)
    Temperature.new(f: temperature)
  end

  def in_fahrenheit
    if @temperature.has_key?(:f)
      @temperature[:f]
    elsif @temperature.has_key?(:c)
      self.c_to_f
    end
  end

  def c_to_f
    @temperature[:c] * 9.0 / 5.0 + 32.0
  end

  def in_celsius
    if @temperature.has_key?(:c)
      @temperature[:c]
    elsif @temperature.has_key?(:f)
      self.f_to_c
    end
  end

  def f_to_c
    (@temperature[:f] - 32.0) * 5.0 / 9.0
  end
end

class Celsius < Temperature
  def initialize(temperature)
    @temperature = {c: temperature}
  end
end

class Fahrenheit < Temperature
  def initialize(temperature)
    @temperature = {f: temperature}
  end
end
